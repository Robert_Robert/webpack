'use strict';

const NODE_ENV = process.env.NODE_ENV || 'development';
var webpack = require('webpack');

module.exports =
    {
        //entry: "./home",
        context: __dirname + '/frontend',
        entry: {
            home: "./home", //files to processing
            about: "./about",
            welcome: "./welcome"
        },
        output: {
            path: __dirname + '/public', //should be absolut path(__dirname)
            filename: "[name].js",
            library: "[name]",  //global variable which contains exports issues
        },
    
        externals: {
          lodash: '_',  
        },

        watch: NODE_ENV == 'development', //rebuild if detect changes
        watchOptions: {
            aggregateTimeout: 300
        },

        devtool: NODE_ENV == 'development' ? "eval" : false, //sourcemaps. should be string | bool

        plugins: [
            new webpack.DefinePlugin({
                NODE_ENV: JSON.stringify(NODE_ENV)
            }),// DefinePlugin is used to create global variables for diferent goals(lang, development/production)
            new webpack.optimize.CommonsChunkPlugin({
                name: 'common',
                minChunks: 2, //Define min amount using file for using. By default, chunk must be using in all files.
//                chunks: ['about', 'home', 'welcome'], // Define which files should be checked to common parts of code
            }), // move to separate file common parts of files. include this common file and after other files.
//            new webpack.ProvidePlugin({
//                jquery: 'jquery',
//            })
        ],

        module: {

            loaders: [{
                test: /\.js$/, //check files extesions
                //include: "" - //check files path
                loader: 'babel-loader', // babel loader used to converting ES-6 to old, supported most browsers js code
               // exclude: /node_modules/, //files that belong to this regexp not processed by babel loader. increase time of build
                include: [__dirname + '/frontend'],
                query: {
                    presets: ['es2015'], //need option
                    plugins: ['babel-plugin-transform-runtime'] // exclude some functions from build.js to modules, minificate build.js file
                }
            },
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader!autoprefixer-loader?browsers=last 2 version',
//                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.(png|jpg|svg|ttf|eot|woff|woff2)$/,
                loader: 'file?name=[path][name].[ext]'
            }
                     ],
//            noParse: wrapRegexp(/\/node_modules\/(jquery\/dist\/jquery.min.js/) //do not search require in this file (not separate) Не потрібно розбирати та шукати require. increase time of build

        },

        resolve: {
            modules: ["node_modules"], //define directory where webpack must to search him plugins
            extensions: ["*", ".js", '.jsx', '.css'] //define plugins extensions
        }
    };

if(NODE_ENV == "production"){
    module.exports.plugins.push(
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                //don't show unreachable variables
                warnings: false,
                drop_console: true,
                unsafe: true
            }
        })
    );
} //minification