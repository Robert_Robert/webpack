webpack - run webpack
webpack --display-modules -v  - show which modules included to other modules
webpack --json --profile >stats.json
webpack --profile --display-modules --display-reasons - show all modules and time to their build

npm install --save-dev css-loader
npm install style-loader --save-dev
npm install autoprefixer-loader --save-dev